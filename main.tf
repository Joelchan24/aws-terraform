# # VPC
# # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
# resource "aws_vpc" "main" {
#   cidr_block = "10.0.0.0/16"
#   tags = {
#     Name = "joelchan_VPC"
#   }
# }

# # Public Subnet
# resource "aws_subnet" "public" {
#   vpc_id                  = aws_vpc.main.id
#   cidr_block              = "10.0.1.0/24"
#   map_public_ip_on_launch = true
#   tags = {
#     Name = "joelchan_Public_Subnet"
#   }
# }



resource "aws_s3_bucket" "bucket-joel24" {
    bucket = "mybucket-joel24-tf"
    acl    = "private"
    tags = {
        Name = "mybucket-tf"
    }
}
